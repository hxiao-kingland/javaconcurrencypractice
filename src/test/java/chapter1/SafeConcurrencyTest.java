package chapter1;

import dev.yavuztas.junit.ConcurrentExtension;
import dev.yavuztas.junit.ConcurrentTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ConcurrentExtension.class)
public class SafeConcurrencyTest {

    static final Sequence CONSTANT_VALUE = new Sequence();
    static final ConcurrentHashMap<String, Object> threads = new ConcurrentHashMap<String, Object>();

    @ConcurrentTest(count = 10000)
    void testConcurrency() {
        String threadId = "Thread#" + Thread.currentThread().getId();
        // using the ConcurrentHashMap help to check whether the test target is thread safe or not.
        // If the test target is thread safe, then the return value should be unique.
        threads.put(String.valueOf(CONSTANT_VALUE.getNext()), threadId);
    }

    @AfterEach
    void testCount() {
        System.out.println("Threads count: " + threads.size());
        System.out.println(threads.values());
        // In our test, we start 10,000 threads then it should have 10,000 unique key in the ConcurrentHashMap.
        // If not equal, then it means the test target is not thread safe.
        assertEquals(10000, threads.size());
    }
}
