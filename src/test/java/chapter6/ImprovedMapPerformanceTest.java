package chapter6;

import chapter4.ImprovedMap;
import chapter5.MapOperator;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class ImprovedMapPerformanceTest {

    @Test
    void givenConcurrentHashMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness();
        System.out.println("ConcurrentHashMap : " + improvedTestHarness.timeTasks(100, new MapOperator(new ConcurrentHashMap())));
    }

    @Test
    void givenSynchronizedMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness();
        System.out.println("SynchronizedMap : " + improvedTestHarness.timeTasks(100, new MapOperator(Collections.synchronizedMap(new HashMap<>()))));
    }

    @Test
    void givenImprovedMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness();
        System.out.println("ImprovedMap : " + improvedTestHarness.timeTasks(100, new MapOperator(new ImprovedMap())));
    }

    // Task 004 Test Result:
    // ConcurrentHashMap : 71583100
    // SynchronizedMap : 71862700
    // ImprovedMap : 71627600

    // Task 005 Test Result:
    // ConcurrentHashMap : 64793000
    // SynchronizedMap : 67538600
    // ImprovedMap : 70895000

    // again
    // ConcurrentHashMap : 78952000
    // SynchronizedMap : 81111600
    // ImprovedMap : 94214100
}
