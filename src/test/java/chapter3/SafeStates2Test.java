package chapter3;

import dev.yavuztas.junit.ConcurrentExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ConcurrentExtension.class)
public class SafeStates2Test {
    static SafeStates2 CONSTANT_VALUE = new SafeStates2();

    @Test
    void testSafeStates2() {
        String[] originalStates = CONSTANT_VALUE.getStates();
        String originalValue1 = originalStates[0];
        String originalValue2 = originalStates[1];
        System.out.println("Before changes : " + Arrays.asList(originalStates));

        String[] states = CONSTANT_VALUE.getStates();
        states[0] = "UAK";
        states[1] = "UAL";

        System.out.println("After changes : " + Arrays.asList(originalStates));
        System.out.println("After changes : " + Arrays.asList(CONSTANT_VALUE.getStates()));

        assertEquals(originalValue1, CONSTANT_VALUE.getStates()[0]);
        assertEquals(originalValue2, CONSTANT_VALUE.getStates()[1]);
    }

}
