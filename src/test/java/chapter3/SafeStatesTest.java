package chapter3;

import dev.yavuztas.junit.ConcurrentExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(ConcurrentExtension.class)
public class SafeStatesTest {
    static SafeStates CONSTANT_VALUE = new SafeStates();

    @Test()
    void testSafeStates() {
        List<String> states = CONSTANT_VALUE.getStates();

        assertThrows(UnsupportedOperationException.class, () -> {
            states.set(0, "UAK");
            states.set(1, "UAL");
        });
    }
}