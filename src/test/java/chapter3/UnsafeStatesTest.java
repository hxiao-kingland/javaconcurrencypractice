package chapter3;

import dev.yavuztas.junit.ConcurrentExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ConcurrentExtension.class)
public class UnsafeStatesTest {
    static UnsafeStates CONSTANT_VALUE = new UnsafeStates();

    @Test
    void testUnsafeStates() {
        String[] originalStates = CONSTANT_VALUE.getStates();
        String originalValue1 = originalStates[0];
        String originalValue2 = originalStates[1];

        String[] states = CONSTANT_VALUE.getStates();
        states[0] = "UAK";
        states[1] = "UAL";

        assertEquals(originalValue1, CONSTANT_VALUE.getStates()[0]);
        assertEquals(originalValue2, CONSTANT_VALUE.getStates()[1]);
    }

}
