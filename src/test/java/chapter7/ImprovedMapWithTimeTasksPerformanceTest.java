package chapter7;

import chapter4.ImprovedMap;
import chapter5.MapOperator;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class ImprovedMapWithTimeTasksPerformanceTest {

    @Test
    void givenConcurrentHashMap_whenOperatorParallel_thenCalculateExecuteTime_will_cancel() throws Exception {
        ImprovedTestHarnessWithTimeTasks target = new ImprovedTestHarnessWithTimeTasks();
        System.out.println("ConcurrentHashMap : " + target.timeTasks(1, 1, new MapOperator(new ConcurrentHashMap())));
    }

    @Test
    void givenConcurrentHashMap_whenOperatorParallel_thenCalculateExecuteTime_do_not_cancel() throws Exception {
        ImprovedTestHarnessWithTimeTasks target = new ImprovedTestHarnessWithTimeTasks();
        System.out.println("ConcurrentHashMap : " + target.timeTasks(1, 2, new MapOperator(new ConcurrentHashMap())));
    }

    // Task 006 Test Result:
    // 1st test
    //    java.util.concurrent.FutureTask@45167172[Not completed, task = java.util.concurrent.Executors$RunnableAdapter@54ffeae3[Wrapped task = chapter5.MapOperator@422d11f0]]was timeout!
    //    java.util.concurrent.FutureTask@45167172[Not completed, task = java.util.concurrent.Executors$RunnableAdapter@54ffeae3[Wrapped task = chapter5.MapOperator@422d11f0]]execute cancel!
    //    ConcurrentHashMap : 1002291100

    // 2nd test
    //    ConcurrentHashMap : 1006522600

}
