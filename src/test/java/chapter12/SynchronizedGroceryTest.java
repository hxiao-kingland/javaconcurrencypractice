package chapter12;

import chapter11.SynchronizedGrocery;
import junit.framework.Test;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;import java.util.concurrent.Phaser;
import java.util.concurrent.TimeoutException;


public class SynchronizedGroceryTest extends JSR166TestCase{

    public static Test suite() {
        return new TestSuite(SynchronizedGroceryTest.class);
    }

    public void testConstructor() {
        SynchronizedGrocery grocery = new SynchronizedGrocery();
        assertEquals(0, grocery.getFruit().size());
        assertEquals(0, grocery.getVegetables().size());
        grocery.addFruit(0, "TestFruit");
        assertEquals(1, grocery.getFruit().size());
        assertEquals(0, grocery.getVegetables().size());
    }

    public void testThreadWithAfterInterrupt() {
        final SynchronizedGrocery grocery = new SynchronizedGrocery();
        assertEquals(0, grocery.getFruit().size());
        assertEquals(0, grocery.getVegetables().size());
        final CountDownLatch pleaseInterrupt = new CountDownLatch(1);

        Thread t = newStartedThread(new CheckedRunnable() {
            public void realRun() {
                Thread.currentThread().interrupt();
                assertEquals(0, grocery.getFruit().size());
                assertEquals(0, grocery.getVegetables().size());
                pleaseInterrupt.countDown();
                grocery.addFruit(0, "TestFruit");
                assertTrue(Thread.currentThread().isInterrupted());
                assertEquals(1, grocery.getFruit().size());
                assertTrue(Thread.currentThread().isInterrupted());
            }});

        await(pleaseInterrupt);
        Thread.currentThread().interrupt();
        assertEquals(1, grocery.getFruit().size());
        assertTrue(Thread.interrupted());
        awaitTermination(t);
    }

    public void testThreadOperation() {
        final SynchronizedGrocery grocery = new SynchronizedGrocery();
        assertEquals(0, grocery.getFruit().size());
        assertEquals(0, grocery.getVegetables().size());
        List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < 10; i++) {
            assertEquals(0, grocery.getFruit().size());
            assertEquals(0, grocery.getVegetables().size());
            threads.add(newStartedThread(new CheckedRunnable() {
                public void realRun() {
                    int currentSize = grocery.getFruit().size();
                    grocery.addFruit(currentSize, "TestFruit" + currentSize);
                }}));
        }

        for (Thread thread : threads)
            awaitTermination(thread);
        assertEquals(10, grocery.getFruit().size());
    }


    public void testPlayWithThreadInterruptFunction() throws InterruptedException {
        final SynchronizedGrocery grocery = new SynchronizedGrocery();
        final CountDownLatch pleaseInterrupt = new CountDownLatch(2);

        Thread t1 = newStartedThread(new CheckedRunnable() {
            public void realRun() {
                Thread.currentThread().interrupt();
                try {
                    throw new InterruptedException();
                } catch (InterruptedException success) {}
                assertTrue(Thread.interrupted());

                pleaseInterrupt.countDown();
                try {
                    throw new InterruptedException();
                } catch (InterruptedException success) {}
                assertFalse(Thread.interrupted());
            }});

        Thread t2 = newStartedThread(new CheckedRunnable() {
            public void realRun() throws TimeoutException {
                Thread.currentThread().interrupt();
                try {
                    throw new InterruptedException();
                } catch (InterruptedException success) {}
                assertTrue(Thread.interrupted());

                pleaseInterrupt.countDown();
                try {
                    throw new InterruptedException();
                } catch (InterruptedException success) {}
                assertFalse(Thread.interrupted());
            }});

        await(pleaseInterrupt);
        t1.interrupt();
        t2.interrupt();
        awaitTermination(t1);
        awaitTermination(t2);
        assertEquals(0, grocery.getFruit().size());
    }
}
