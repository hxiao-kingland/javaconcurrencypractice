package chapter5;

import chapter4.ImprovedMap;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class MapPerformanceTest {

    @Test
    void givenConcurrentHashMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        TestHarness testHarness = new TestHarness();
        System.out.println("ConcurrentHashMap : " + testHarness.timeTasks(100, new MapOperator(new ConcurrentHashMap())));
    }

    @Test
    void givenSynchronizedMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        TestHarness testHarness = new TestHarness();
        System.out.println("SynchronizedMap : " + testHarness.timeTasks(100, new MapOperator(Collections.synchronizedMap(new HashMap<>()))));
    }

    @Test
    void givenImprovedMap_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        TestHarness testHarness = new TestHarness();
        System.out.println("ImprovedMap : " + testHarness.timeTasks(100, new MapOperator(new ImprovedMap())));
    }

    // Test Result:
    // ConcurrentHashMap : 71583100
    // SynchronizedMap : 71862700
    // ImprovedMap : 71627600
}
