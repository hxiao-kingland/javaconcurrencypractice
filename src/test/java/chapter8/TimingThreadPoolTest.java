package chapter8;

import chapter5.MapOperator;
import chapter6.ImprovedTestHarness;
import org.junit.jupiter.api.Test;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

public class TimingThreadPoolTest {

    @Test
    void givenConcurrentHashMap_andTimingThreadPool_withStartAllCoreThread_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        ExecutorService executorService = new TimingThreadPool(10, true);
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness(executorService);
        System.out.println("ConcurrentHashMap : " + improvedTestHarness.timeTasks(10, new MapOperator(new ConcurrentHashMap())));
    }

    @Test
    void givenConcurrentHashMap_andTimingThreadPool_withoutStartAllCoreThread_whenOperatorParallel_thenCalculateExecuteTime() throws Exception {
        ExecutorService executorService = new TimingThreadPool(10, false);
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness(executorService);
        System.out.println("ConcurrentHashMap : " + improvedTestHarness.timeTasks(10, new MapOperator(new ConcurrentHashMap())));
    }

    // Task 007 Test Result:
    // withStartAllCoreThread' avg time    :   1007556170ns
    // withoutStartAllCoreThread' avg time :   1008238160ns
    // The start all core thread has a little better performance but when there have many threads to run, it almost to be same data level.
}
