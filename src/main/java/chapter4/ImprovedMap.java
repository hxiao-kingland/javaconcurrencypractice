package chapter4;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

import java.util.*;
import java.util.function.BiFunction;

/**
 * @author hxiao
 */
@ThreadSafe
public class ImprovedMap<K,V> implements Map<K,V> {
    @GuardedBy("lock")
    private final HashMap<K,V> map;

    private final Object lock;

    public ImprovedMap() {
        this.map = new HashMap<>();
        this.lock = new Object();
    }

    ImprovedMap(Map<K,V> map) {
        this.map = new HashMap<K, V>();
        // using the map's putAll to implement a map's shadow copy
        this.map.putAll(map);
        this.lock = new Object();
    }

    ImprovedMap(Map<K,V> map, Object lock) {
        // using the clone to implement a map's shadow copy
        this.map = (HashMap<K, V>) ((HashMap)map).clone();
        this.lock = lock;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public V get(Object key) {
        synchronized (lock) {return map.get(key);}
    }

    @Override
    public V put(K key, V value) {
        synchronized (lock) {return map.put(key, value);}
    }

    @Override
    public V computeIfPresent(K key,
                              BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        synchronized (lock) {return map.computeIfPresent(key, remappingFunction);}
    }

    @Override
    public V remove(Object key) {
        return map.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        map.putAll(m);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Set<K> keySet() {
        return map.keySet();
    }

    @Override
    public Collection<V> values() {
        return map.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return map.entrySet();
    }


    public Map<K, V> shallowCopy(Map<K, V> map) {
        return Collections.unmodifiableMap(new HashMap<K, V>(map));
    }

    public Map<K, V> deepCopy(Map<K, V> map) {
        Map result = new HashMap<K, V>();

        for (K key: map.keySet()) {
            result.put(key, map.get(key));
        }
        return Collections.unmodifiableMap(result);
    }


    public static <V> V create(Class<V> clazz) {
        V v = null;
        try {
            v = clazz.newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return v;
    }
}
