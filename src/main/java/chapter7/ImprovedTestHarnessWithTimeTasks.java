package chapter7;

import java.util.concurrent.*;

/**
 * @author hxiao
 */
public class ImprovedTestHarnessWithTimeTasks implements TimeTasks {

    @Override
    public long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task) throws InterruptedException, BrokenBarrierException {
        final CyclicBarrier startGate = new CyclicBarrier (nThreads + 1);
        final CyclicBarrier  endGate = new CyclicBarrier (nThreads + 1);
        final ExecutorService executorService = Executors.newFixedThreadPool(nThreads);

        try {
            // start all threads with using CyclicBarrier as the startGate and endGate
            for (int i = 0; i < nThreads; i++) {
                executorService.execute(() -> {
                        try {
                            startGate.await();
                            // create a single thread executor which can submit the Future task with timeout
                            ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
                            Future<?> withTimeTask = singleThreadExecutor.submit(task);
                            try {
                                withTimeTask.get(timeoutInSeconds, TimeUnit.SECONDS);
                            } catch (ExecutionException e) {
                                throw new RuntimeException(e);
                            } catch (TimeoutException e) {
                                // When the task was timeout
                                System.out.println(withTimeTask.toString() + "was timeout!");
                            } finally {
                                endGate.await();
                                // Only call cancel when the task was not completed
                                if (!withTimeTask.isDone()) {
                                    System.out.println(withTimeTask.toString() + "execute cancel!");
                                    withTimeTask.cancel(true);
                                }
                                singleThreadExecutor.shutdown();
                            }
                        } catch (BrokenBarrierException | InterruptedException ignored) {

                        }
                });
            }

            // To ensure all thread are start before actual testing
            startGate.await();
            long start = System.nanoTime();

            // To ensure all thread have completed processing
            endGate.await();
            long end = System.nanoTime();

            // only evaluate the multiple thread run time
            return end - start;
        } finally {
            if (!executorService.isShutdown()) {
                executorService.shutdown();
            }
        }
    }
}