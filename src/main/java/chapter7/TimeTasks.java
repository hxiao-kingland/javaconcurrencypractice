package chapter7;

import java.util.concurrent.BrokenBarrierException;

/**
 * @author hxiao
 */
public interface TimeTasks {

    /**
     * Run the runnable task in a sandbox with the designed threads number and limit time
     * @param nThreads the thread numbers
     * @param timeoutInSeconds the limit time in seconds
     * @param task the runnable task
     * @return the running times
     */
    public long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task) throws InterruptedException, BrokenBarrierException;
}
