package chapter8;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * @author hxiao
 */
public class TimingThreadPool extends ThreadPoolExecutor {
    private final ThreadLocal<Long> startTime = new ThreadLocal<>();
    private static final Logger logger = Logger.getLogger("TimingThreadPool");
    private final AtomicLong numTasks = new AtomicLong();
    private final AtomicLong totalTime = new AtomicLong();

    private volatile boolean shouldStartAllCoreThreads;

    public TimingThreadPool(int corePoolSize, boolean shouldStartAllCoreThreads) {
        super(corePoolSize, corePoolSize * 3, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<>(200));
        this.shouldStartAllCoreThreads = shouldStartAllCoreThreads;
        initializeThreadPool();
    }

    /**
     * Apply the additional initialize action for the thread pool
     */
    private void initializeThreadPool() {
        if (shouldStartAllCoreThreads) {
            long startTime = System.nanoTime();
            System.out.println(String.format("InitializeThreadPool with %s core threads: start at %s", this.getCorePoolSize(), startTime));
            this.prestartAllCoreThreads();
            long endTime = System.nanoTime();
            System.out.println(String.format("InitializeThreadPool with %s core threads: End at %s, and takes %s", this.getCorePoolSize(), endTime, startTime - endTime));
        }
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        logger.fine(String.format("Thread %s : start %s", t, r));
        System.out.println(String.format("Thread %s : start %s", t, r));
        startTime.set(System.nanoTime());
    }

    @Override
    protected void afterExecute(Runnable t, Throwable r) {
        try {
            long endTime = System.nanoTime();
            long taskTime = endTime - startTime.get();
            numTasks.incrementAndGet();
            totalTime.addAndGet(taskTime);
            logger.fine(String.format("Thread %s  : end %s, time %dns", t, r, taskTime));
        } finally {
            super.afterExecute(t, r);
        }
    }

    @Override
    protected void terminated() {
        try {
            logger.info(String.format("Terminated : avg time = %dns", totalTime.get() / numTasks.get()));
        } finally {
            super.terminated();
        }
    }
}
