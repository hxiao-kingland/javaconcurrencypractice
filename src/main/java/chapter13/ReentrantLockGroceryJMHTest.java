package chapter13;
s'ch'
import chapter11.Grocery;
import chapter11.SynchronizedGrocery;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author hxiao
 */
@State(Scope.Benchmark)
public class ReentrantLockGroceryJMHTest {

    @Benchmark
    public void testSynchronizedGroceryWithJMH() {
        SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery();

        // operate SynchronizedGrocery with multiple thread
        operateOnGroceryWithMultipleThread(synchronizedGrocery);
    }

    @Benchmark
    public void testNonFairLockGroceryWithJMH() {
        NonFairLockGrocery nonFairLockGrocery = new NonFairLockGrocery();

        // operate NonFairLockGrocery with multiple thread
        operateOnGroceryWithMultipleThread(nonFairLockGrocery);
    }

    @Benchmark
    public void testFairLockGroceryWithJMH() {
        FairLockGrocery fairLockGrocery = new FairLockGrocery();

        // operate FairLockGrocery with multiple thread
        operateOnGroceryWithMultipleThread(fairLockGrocery);
    }


    private void operateOnGroceryWithMultipleThread(Grocery grocery) {
        // run in thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        // add fruit
        executorService.execute(() -> {
            for (int j = 0; j < 100; j++) {
                grocery.addFruit(j, "Fruit" + j);
                grocery.addVegetable(j, "Vegetable" + j);
            }
        });

        executorService.shutdown();
    }
}

//  Performance test 1 - SynchronizedGrocery
//    Benchmark                                                    Mode  Cnt     Score      Error  Units
//    ReentrantLockGroceryJMHTest.testSynchronizedGroceryWithJMH  thrpt   25  5802.216 ± 1898.512  ops/s

//  Performance test 2 - NonFairLockGrocery
//    Benchmark                                                    Mode  Cnt     Score      Error  Units
//    ReentrantLockGroceryJMHTest.testNonFairLockGroceryWithJMH   thrpt   25  5833.821 ± 1470.067  ops/s

//  Performance test 3 - FairLockGrocery
//    Benchmark                                                    Mode  Cnt     Score      Error  Units
//    ReentrantLockGroceryJMHTest.testFairLockGroceryWithJMH      thrpt   25  5832.028 ± 1155.980  ops/s

// Test Result
// NonFairLockGrocery >> FairLockGrocery >> SynchronizedGrocery