package chapter13;

import chapter11.Grocery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FairLockGrocery implements Grocery {

    Lock fairLock = new ReentrantLock(true);

    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    @Override
    public void addFruit(int index, String fruit) {
        fairLock.lock();
        try {
            fruits.add(index, fruit);
        } finally {
            fairLock.unlock();
        }
    }

    @Override
    public void addVegetable(int index, String vegetable) {
        fairLock.lock();
        try {
            vegetables.add(index, vegetable);
        } finally {
            fairLock.unlock();
        }
    }

    @Override
    public List<String> getFruit() {
        fairLock.lock();
        try {
            return Collections.unmodifiableList(fruits);
        } finally {
            fairLock.unlock();
        }
    }

    @Override
    public List<String> getVegetables() {
        fairLock.lock();
        try {
            return Collections.unmodifiableList(vegetables);
        } finally {
            fairLock.unlock();
        }
    }
}
