package chapter13;

import chapter11.Grocery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NonFairLockGrocery implements Grocery {
    Lock nonFairLock = new ReentrantLock(false);

    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    @Override
    public void addFruit(int index, String fruit) {
        nonFairLock.lock();
        try {
            fruits.add(index, fruit);
        } finally {
            nonFairLock.unlock();
        }
    }

    @Override
    public void addVegetable(int index, String vegetable) {
        nonFairLock.lock();
        try {
            vegetables.add(index, vegetable);
        } finally {
            nonFairLock.unlock();
        }
    }

    @Override
    public List<String> getFruit() {
        nonFairLock.lock();
        try {
            return Collections.unmodifiableList(fruits);
        } finally {
            nonFairLock.unlock();
        }
    }

    @Override
    public List<String> getVegetables() {
        nonFairLock.lock();
        try {
            return Collections.unmodifiableList(vegetables);
        } finally {
            nonFairLock.unlock();
        }
    }
}
