package chapter3;

public class SafeStates2 {
    private AtomicArray<String> states;

    public SafeStates2() {
        states = new AtomicArray(2);
        states.set(0, "AK");
        states.set(1, "AL");
    }

    public String[] getStates(){
        String[] copyValues = new String[states.length()];
        return states.toArray(copyValues);
    }
}
