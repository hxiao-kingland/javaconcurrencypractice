package chapter3;

public class UnsafeStates {
    private volatile String[] states = new String[] {
      "AK", "AL"
    };

    public String[] getStates() {
        return states;
    }
}
