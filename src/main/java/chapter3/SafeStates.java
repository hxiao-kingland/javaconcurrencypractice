package chapter3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SafeStates {
    private String[] states = new String[] {
            "AK", "AL"
    };

    public List<String> getStates() {
        return Collections.unmodifiableList(Arrays.asList(states));
    }
}
