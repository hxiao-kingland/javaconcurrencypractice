package chapter1;

import net.jcip.annotations.NotThreadSafe;

/**
 * @author hxiao
 */
@NotThreadSafe
public class UnsafeSequence {
    private int value;

    /** return a unique value. */
    public int getNext() {
        return value++;
    }
}
