package chapter1;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * @author hxiao
 */
@ThreadSafe
public class Sequence {
    @GuardedBy("this")
    private int Value;

    public synchronized int getNext() {
        return Value++;
    }
}
