package chapter11;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author hxiao
 */
@State(Scope.Benchmark)
public class GroceryJMHTest {

    @Benchmark
    public void testSynchronizedGroceryWithJMH() {
        SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery();

        // operate SynchronizedGrocery with multiple thread
        operateOnGroceryWithMultipleThread(synchronizedGrocery);
    }

    @Benchmark
    public void testReduceLockScopeSynchronizedGroceryWithJMH() {
        ReduceLockScopeSynchronizedGrocery reduceLockScopeSynchronizedGrocery = new ReduceLockScopeSynchronizedGrocery();

        // operate ReduceLockScopeSynchronizedGrocery with multiple thread
        operateOnGroceryWithMultipleThread(reduceLockScopeSynchronizedGrocery);
    }

    @Benchmark
    public void testConcurrentHashMapGroceryWithJMH() {
        ConcurrentHashMapGrocery concurrentHashMapGrocery = new ConcurrentHashMapGrocery();

        // operate ConcurrentHashMapGrocery with multiple thread
        operateOnGroceryWithMultipleThread(concurrentHashMapGrocery);
    }


    private void operateOnGroceryWithMultipleThread(Grocery grocery) {
        // run in thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        // add fruit
        executorService.execute(() -> {
            for (int j = 0; j < 100; j++) {
                grocery.addFruit(j, "Fruit" + j);
                grocery.addVegetable(j, "Vegetable" + j);
            }
        });

        executorService.shutdown();
    }
}

//  Performance test 1 - SynchronizedGrocery
//    Benchmark                                       Mode  Cnt     Score      Error  Units
//    GroceryJMHTest.testSynchronizedGroceryWithJMH  thrpt   25  4440.478 ± 2666.857  ops/s

//  Performance test 2 - ReduceLockScopeSynchronizedGrocery
//    Benchmark                                                      Mode  Cnt     Score      Error  Units
//    GroceryJMHTest.testReduceLockScopeSynchronizedGroceryWithJMH  thrpt   25  5840.651 ± 2359.754  ops/s

//  Performance test 3 - ConcurrentHashMapGrocery
//    Benchmark                                            Mode  Cnt     Score      Error  Units
//    GroceryJMHTest.testConcurrentHashMapGroceryWithJMH  thrpt   25  4772.924 ± 2527.301  ops/s