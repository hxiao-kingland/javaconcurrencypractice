package chapter11;

import java.util.List;

/**
 * @author hxiao
 */
public interface Grocery {
    void addFruit(int index, String fruit);
    void addVegetable(int index, String vegetable);

    List<String> getFruit();
    List<String> getVegetables();
}
