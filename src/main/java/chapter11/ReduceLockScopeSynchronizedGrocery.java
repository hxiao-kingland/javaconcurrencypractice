package chapter11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author hxiao
 */
public class ReduceLockScopeSynchronizedGrocery implements Grocery {
    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    @Override
    public void addFruit(int index, String fruit) {
        synchronized(fruits) {
            fruits.add(index, fruit);
        }
    }

    @Override
    public synchronized void addVegetable(int index, String vegetable) {
        synchronized(vegetables) {
            vegetables.add(index, vegetable);
        }
    }

    @Override
    public List<String> getFruit() {
        return Collections.unmodifiableList(fruits);
    }

    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList(vegetables);
    }
}
