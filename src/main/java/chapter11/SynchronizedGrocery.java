package chapter11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author hxiao
 */
public class SynchronizedGrocery implements Grocery {
    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    @Override
    public synchronized void addFruit(int index, String fruit) {
        fruits.add(index, fruit);
    }

    @Override
    public synchronized void addVegetable(int index, String vegetable) {
        vegetables.add(index, vegetable);
    }

    @Override
    public List<String> getFruit() {
        return Collections.unmodifiableList(fruits);
    }

    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList(vegetables);
    }
}
