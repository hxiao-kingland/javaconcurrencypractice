package chapter11;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hxiao
 */
public class ConcurrentHashMapGrocery implements Grocery {
    private final Map<Integer, String> fruits = new ConcurrentHashMap<Integer, String>();
    private final Map<Integer, String> vegetables = new ConcurrentHashMap<Integer, String>();

    @Override
    public void addFruit(int index, String fruit) {
        String result = fruits.putIfAbsent(index, fruit);

        if (result == null) {
            //System.out.println("Means the map do not have this key, and have add it to the map.");
        } else {
            System.out.println("If see this, then means it is not thread safe!!!");
            // means it is not a thread safe way
            System.exit(-1);
        }
    }

    @Override
    public synchronized void addVegetable(int index, String vegetable) {

        String result = vegetables.putIfAbsent(index, vegetable);

        if (result == null) {
            //System.out.println("Means the map do not have this key, and have add it to the map.");
        } else {
            System.out.println("If see this, then means it is not thread safe!!!");
            // means it is not a thread safe way
            System.exit(-1);
        }
    }

    @Override
    public List<String> getFruit() {
        return Collections.unmodifiableList((List<String>) (fruits.values()));
    }

    @Override
    public List<String> getVegetables() {
        return Collections.unmodifiableList((List<String>) (vegetables.values()));
    }
}
