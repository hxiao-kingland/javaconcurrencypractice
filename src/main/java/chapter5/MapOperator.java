package chapter5;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * @author hxiao
 */
public class MapOperator implements Runnable{

    private final Map map;

    public MapOperator(Map map) {
        this.map = map;
    }

    @Override
    public void run() {
        // change to run in thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 4; i++) {
            executorService.execute(() -> {
                for (int j = 0; j < 100; j++) {
                    int value = ThreadLocalRandom.current().nextInt(10000);
                    String key = String.valueOf(value);
                    map.put(key, value);
                    map.get(key);
                }
            });
        }

        // For test the timeTasks, add a sleep 1 second to ensure the thread can't be completed in 1 second
        // So we can test with 1 second for the timeout case, and with 2 second should not encounter the timeout.
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        executorService.shutdown();
    }
}
