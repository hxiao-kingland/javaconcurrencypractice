package chapter5;

import chapter4.ImprovedMap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author hxiao
 */
@State(Scope.Benchmark)
public class MapJMHTest {

    @Benchmark
    public void testConcurrentHashMapWithJMH() {
        Map<String, Integer> testMap = new ConcurrentHashMap<>();
        // change to operate map with multiple thread
        operateOnMapWithMultipleThread(testMap);
    }

    @Benchmark
    public void testSynchronizedMapWithJMH() {
        Map<String, Integer> testMap = Collections.synchronizedMap(new HashMap<>());
        // change to operate map with multiple thread
        operateOnMapWithMultipleThread(testMap);
    }

    @Benchmark
    public void testImprovedMapWithJMH() {
        Map<String, Integer> testMap = new ImprovedMap<>();

        // change to operate map with multiple thread
        operateOnMapWithMultipleThread(testMap);
    }

    private void operateOnMapWithMultipleThread(Map map) {
        // change to run in thread pool
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 4; i++) {
            executorService.execute(() -> {
                for (int j = 0; j < 100; j++) {
                    int value = ThreadLocalRandom.current().nextInt(10000);
                    String key = String.valueOf(value);
                    map.put(key, value);
                    map.get(key);
                }
            });
        }
        executorService.shutdown();
    }

//    JMH Test Result:
//    Benchmark                                 Mode  Cnt     Score     Error  Units
//    MapJMHTest.testConcurrentHashMapWithJMH  thrpt   25  1418.955 ± 450.653  ops/s
//
//    Benchmark                               Mode  Cnt         Score         Error  Units
//    MapJMHTest.testSynchronizedMapWithJMH    thrpt   25  1171.664 ± 690.380  ops/s
//
//    Benchmark                           Mode  Cnt         Score         Error  Units
//    MapJMHTest.testImprovedMapWithJMH        thrpt   25  599.252 ± 335.058  ops/s

//    ConcurrentHashMap > SynchronizedMap > ImprovedMap
}
