package chapter10;

import java.util.concurrent.TimeUnit;

public class LeftRightDeadLock {
    private final Object left = new Object();
    private final Object right = new Object();

    public void leftRight() {
        synchronized (left) {
            try  {
                TimeUnit.SECONDS.sleep( 3 );
            }  catch  (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (right) {
                doSomething();
            }
        }
    }

    public void rightLeft() {
        synchronized (right) {
            try  {
                TimeUnit.SECONDS.sleep( 3 );
            }  catch  (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (left) {
                doSomethingElse();
            }
        }
    }

    private void doSomething() {
        System.out.println("Enter doSomething() method");
    }

    private void doSomethingElse() {
        System.out.println("Enter doSomethingElse() method");
    }

    public static void main(String[] args) {
        final LeftRightDeadLock deadLock = new LeftRightDeadLock();

        Thread t1 = new Thread(new Runnable(){
            @Override
            public void run(){
                deadLock.leftRight();
            }
        });

        Thread t2 = new Thread(new Runnable(){
            @Override
            public void run(){
                deadLock.rightLeft();
            }
        });

        t1.start();
        t2.start();
    }
}