package chapter6;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author hxiao
 */
public class ImprovedTestHarness {
    final ExecutorService executorService;
    public ImprovedTestHarness() {
        this.executorService = Executors.newFixedThreadPool(100);
    }

    public ImprovedTestHarness(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public long timeTasks(int nThreads, final Runnable task) throws InterruptedException, BrokenBarrierException {
        final CyclicBarrier startGate = new CyclicBarrier (nThreads + 1);
        final CyclicBarrier  endGate = new CyclicBarrier (nThreads + 1);

        try {
            // start all threads with using CyclicBarrier as the startGate and endGate
            for (int i = 0; i < nThreads; i++) {
                executorService.execute(() -> {
                        try {
                            startGate.await();
                            try {
                                task.run();
                            } finally {
                                endGate.await();
                            }
                        } catch (BrokenBarrierException | InterruptedException ignored) {}
                });
            }

            // To ensure all thread are start before actual testing
            startGate.await();
            long start = System.nanoTime();

            // To ensure all thread have completed processing
            endGate.await();
            long end = System.nanoTime();

            // only evaluate the multiple thread run time
            return end - start;
        } finally {
            if (!executorService.isShutdown()) {
                executorService.shutdown();
            }
        }
    }
}